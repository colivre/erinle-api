Erinle Back-end
===============

It's the Erinle API server.

To configure, run:
```bash
$ ./scripts/quick-start
```
You will need RVM. Get it at [rvm.io](https://rvm.io).


To run a development server, run:
```bash
$ ./scripts/run-dev
```

Production
==========

You must install all Gems in the sistem. Do not use RVM or will be hard to use the cron script.

To enable automatic dosages notification, you must make two links:
```bash
ln -s /path/to/erinle-api/scripts/create-notifications /usr/local/bin/erinle-create-notifications
cp /path/to/erinle-api/etc/create-notifications.cron /etc/cron.d/erinle-create-notifications
ln -s /path/to/erinle-api/bin/init_erinle-api /etc/init.d/erinle-api
ln -s /path/to/erinle-api/etc/erinle-api.service /lib/systemd/system/
ln -s /path/to/erinle-api/etc/erinle-api.logrotate /etc/logrotate.d/erinle-api
systemctl daemon-reload
systemctl enable erinle-api
```

**ALERT** Please check and adapt etc/erinle-api.logrotate to point to the right
log path, by default erinle-api.logrotate expects logs at /opt/erinle-api/log/.

Auto start on system boot
-------------------------

TODO: a script to /etc/rc2.d/


Schedule SMS services
=====================

Clear the Erinle's user cron and then write a new one.

```bash
crontab -r && whenever --update-crontab
```

It must be done when you add a new SMS service configuration, like Twilio or Marktel.

If you wont to clear the user's cron, remove the `crontab -r` command, and clear old Whenever tasks by hand.

Creating new system administrators
==================================

Go to erinle root dir and run:

```bash
RAILS_ENV=production ./script/create-adm
```

Login as admin on https://memo.ufba.br
