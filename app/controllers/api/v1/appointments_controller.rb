class Api::V1::AppointmentsController < ApplicationController
  before_action :authenticate_collaborator

  def index
    filter = {}
    filter[:pacient] = params[:pacient] if params[:pacient]
    #filter[:collaborator] = params[:collaborator] if params[:collaborator]
    @appointments = Appointment.where(filter).order 'run_at DESC'
    @appointments = @appointments.limit(params[:limit]) if params[:limit]
    @appointments = @appointments.offset(params[:offset]) if params[:offset]
    render json: { appointments: @appointments, reqLimit: params[:limit] }
  end

  def show
    @appointment = Appointment.find(params[:id])
    render json: @appointment
  end

  def create
    @appointment = Appointment.create(appointment_params)
    if @appointment.persisted?
      render json: @appointment
      @appointment.update_notification_pair
    else
      render json: { errors: @appointment.errors.full_messages }, status: 422
    end
  end

  def update
    @appointment = Appointment.find(params[:id])
    if @appointment.update(appointment_params)
      render json: @appointment
      @appointment.update_notification_pair
    else
      render json: { errors: @appointment.errors.full_messages }, status: 422
    end
  end

  def destroy
    @appointment = Appointment.find(params[:id])
    if @appointment.destroy
      render json: @appointment
    else
      render json: { errors: @appointment.errors.full_messages }, status: 422
    end
  end

  private

  def appointment_params
    params.require(:appointment).permit(
      :run_at, :notification_type, :description, :notes, :status,
      :collaborator_id, :pacient_id
    )
  end

end
