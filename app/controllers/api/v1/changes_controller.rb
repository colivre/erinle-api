class Api::V1::ChangesController < ApplicationController
  before_action :authenticate_collaborator

  def index
    filter = params[:filter] || {}
    filter[:whodunnit] = '' if filter[:whodunnit] =~ /^%[ %]*%$/
    to = Time.parse filter[:period_end] || Time.now.to_s
    from = Time.parse filter[:period_ini] || (to-24*60*60).to_s
    uids = Collaborator.where('name LIKE ?', filter[:whodunnit]).map {|u| u.id.to_s}
    @versions = PaperTrail::Version.where(created_at: from..to)
    @versions = @versions.where item_type: filter[:item_type].capitalize unless filter[:item_type].blank?
    @versions = @versions.where item_id: filter[:item_id] unless filter[:item_id].blank?
    @versions = @versions.where 'whodunnit in (?)', uids unless filter[:whodunnit].blank?
    @versions = @versions.limit(params[:limit]) if params[:limit]
    @versions = @versions.offset(params[:offset]) if params[:offset]
    render json: @versions
  end

  def show
    @version = PaperTrail::Version.find(params[:id])
    render json: @version
  end

end
