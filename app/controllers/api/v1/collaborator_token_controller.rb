class Api::V1::CollaboratorTokenController < Knock::AuthTokenController

  private

  def auth_params
    params.require(:auth).permit :username, :password
  end
end
