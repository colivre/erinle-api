class Api::V1::CollaboratorsController < ApplicationController
  before_action :authenticate_collaborator
  before_action do
    unless current_collaborator.administrator
      render json: { errors: ['You are not an admin'] }, status: 401
    end
  end

  def index
    @collaborators = Collaborator.order(:name).all
    render json: @collaborators
  end

  def autocomplete_data
    render json: Collaborator.all.to_json(:only => [ :id, :name, :nin ])
  end

  def show
    @collaborator = Collaborator.find(params[:id])
    render json: @collaborator
  end

  def create
    @collaborator = Collaborator.new(collaborator_params)
    @collaborator.password = params['password']
    @collaborator.password_confirmation = params['password_confirmation']
    @collaborator.save
    if @collaborator.persisted?
      render json: @collaborator
    else
      render json: { errors: @collaborator.errors.full_messages }, status: 422
    end
  end

  def update
    @collaborator = Collaborator.find(params[:id])
    if @collaborator.update(collaborator_params)
      render json: @collaborator
    else
      render json: { errors: @collaborator.errors.full_messages }, status: 422
    end
  end

  def destroy
    @collaborator = Collaborator.find(params[:id])
    if @collaborator.destroy
      render json: @collaborator
    else
      render json: { errors: @collaborator.errors.full_messages }, status: 422
    end
  end

  private

  def collaborator_params
    if params[:collaborator][:picture] == 'undefined'
      Rails.logger.warn 'The client is sending "undefined" as picture value. JS Error?'
      params[:collaborator].delete :picture
    end
    params.require(:collaborator).permit(:name, :nin, :username, :password, :password_confirmation, :picture, :information, :administrator, :enabled)
  end

end
