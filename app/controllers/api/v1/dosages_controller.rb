class Api::V1::DosagesController < ApplicationController
  before_action :authenticate_collaborator

  def index
    id_filter = {}
    id_filter[:pacient] = params[:pacient] if params[:pacient]
    id_filter[:drug] = params[:drug] if params[:drug]
    columns = []
    values = []
    (params[:filter] || []).each do |key, val|
      columns << key.gsub(/[^a-z._]/, '') + ' ILIKE ?'
      values << val
    end
    @dosages = Dosage.joins(:pacient).joins(:drug).where(columns.join(' AND '), *values).where(id_filter)
    @dosages = @dosages.limit(params[:limit]) if params[:limit]
    @dosages = @dosages.offset(params[:offset]) if params[:offset]
    @dosages = @dosages.order 'end_at DESC'
    render json: { dosages: @dosages, reqLimit: params[:limit] }
  end

  def show
    @dosage = Dosage.find(params[:id])
    render json: @dosage
  end

  def update_future_notifications
    four_hours = (Time.now + 60*60*4).strftime('%F %H:%M UTC').to_time
    now = Time.now.strftime('%F %H:00 UTC').to_time
    @dosage.notifications.where('run_at >= ?', now).destroy_all
    @dosage.reload
    @dosage.create_notifications now, four_hours
  end

  def create
    @dosage = Dosage.create(parse_params dosage_params)
    if @dosage.persisted?
      update_future_notifications
      render json: @dosage
    else
      render json: { errors: @dosage.errors.full_messages }, status: 422
    end
  end

  def update
    @dosage = Dosage.find(params[:id])
    if @dosage.update(parse_params dosage_params)
      update_future_notifications
      render json: @dosage
    else
      render json: { errors: @dosage.errors.full_messages }, status: 422
    end
  end

  def destroy
    @dosage = Dosage.find(params[:id])
    if @dosage.destroy
      render json: @dosage
    else
      render json: { errors: @dosage.errors.full_messages }, status: 422
    end
  end

  private

  def parse_params(params)
    new_params = {}
    params.keys.each do |key|
      new_params[key] = params[key]
      if key.to_s[0..3] == 'cron'
        new_params[key] = params[key].tr('/','-').split(',').map {|m| m.to_i}
      end
      if key.to_s == 'total_dosages' && params[key].blank?
        new_params[key] = 0
      end
    end
    new_params
  end

  def dosage_params
    params.require(:dosage).permit(
      :start_at, :end_at, :total_dosages, :amount, :amount_unit, :description,
      :cron_minute, :cron_hour, :cron_m_day, :cron_w_day, :cron_month,
      :notes, :status, :pacient_id, :drug_id,
    )
  end

end
