class Api::V1::DrugsController < ApplicationController
  before_action :authenticate_collaborator

  def index
    columns = []
    values = []
    (params[:filter] || []).each do |key, val|
      columns << key.gsub(/[^a-z]/, '') + ' ILIKE ?'
      values << val
    end
    @drugs = Drug.where(columns.join(' AND '), *values).order(:name)
    @drugs = @drugs.limit(params[:limit]) if params[:limit]
    @drugs = @drugs.offset(params[:offset]) if params[:offset]
    render json: @drugs
  end

  def autocomplete_data
    render json: Drug.all.to_json(:only => [ :id, :name, :concentration, :form ])
  end

  def show
    @drug = Drug.find(params[:id])
    render json: @drug
  end

  def create
    @drug = Drug.create(drug_params)
    if @drug.persisted?
      render json: @drug
    else
      render json: { errors: @drug.errors.full_messages }, status: 422
    end
  end

  def update
    @drug = Drug.find(params[:id])
    if @drug.update(drug_params)
      render json: @drug
    else
      render json: { errors: @drug.errors.full_messages }, status: 422
    end
  end

  def destroy
    @drug = Drug.find(params[:id])
    if @drug.destroy
      render json: @drug
    else
      render json: { errors: @drug.errors.full_messages }, status: 422
    end
  end

  def search
    @drugs = Drug.search(params[:q])
  end

  private

  def drug_params
    if params[:drug][:image] == 'undefined'
      Rails.logger.warn 'The client is sending "undefined" as image value. JS Error?'
      params[:drug].delete :image
    end
    params.require(:drug).permit(:name, :concentration, :form, :description, :image)
  end

end
