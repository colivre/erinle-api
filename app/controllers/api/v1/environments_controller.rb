class Api::V1::EnvironmentsController < ApplicationController

  def index
    @environment = Environment.first!
    render json: @environment.to_json(only: :name, methods: [:logo_url, :favicon_url])
  end

  def summary
    render json: {
      pacients: Pacient.count,
      drugs: Drug.count,
      dosages_in_treatment: Dosage.count('status = 0'),
      collaborators: Collaborator.count,
    }
  end

end
