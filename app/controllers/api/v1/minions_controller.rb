class Api::V1::MinionsController < ApplicationController
  before_action :authenticate_collaborator
  before_action do
    unless current_collaborator.administrator
      render json: { errors: ['You are not an admin'] }, status: 401
    end
  end

  def index
    @minions = Minion.all
    render json: @minions
  end

  def show
    @minion = Minion.find(params[:id])
    render json: @minion
  end

  def create
    @minion = Minion.create(minion_params)
    if @minion.persisted?
      render json: @minion
    else
      render json: { errors: @minion.errors.full_messages }, status: 422
    end
  end

  def update
    @minion = Minion.find(params[:id])
    if @minion.update(minion_params)
      render json: @minion
    else
      render json: { errors: @minion.errors.full_messages }, status: 422
    end
  end

  def destroy
    @minion = Minion.find(params[:id])
    if @minion.destroy
      render json: @minion
    else
      render json: { errors: @minion.errors.full_messages }, status: 422
    end
  end

  private

  def minion_params
    params.require(:minion).permit(:key, :description, :enabled)
  end

end
