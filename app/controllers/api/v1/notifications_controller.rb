class Api::V1::NotificationsController < ApplicationController

  adm_actions = [:index, :create, :destroy]

  before_action :authenticate_collaborator, only: adm_actions

  before_action do
    unless adm_actions.include? params[:action].to_sym
      if params[:key].blank? && params[:pacientkey].blank?
        head :forbidden
      else
        if params[:key]
          @minion = Minion.enabled.find_by key: params[:key]
          if @minion.present?
            @minion.touch(:last_request_at)
          else
            head :forbidden
          end
        else # has pacientkey
          @pacient = Pacient.find_by app_key: params[:pacientkey]
          if @pacient.present?
            true #TODO: make @pacient.touch(:last_app_request_at)
          else
            head :forbidden
          end
        end
      end
    end
  end

  def index
    filter = params[:filter] || {}
    d_ini = filter[:period_ini] || Time.now.strftime('%F %H:%M UTC').to_time - 1*60*60
    d_end = filter[:period_end] || Time.now.strftime('%F %H:%M UTC').to_time + 4*60*60
    @notifications = Notification.where ['run_at >= ? AND run_at <= ?', d_ini, d_end]
    @notifications = @notifications.where minion_id: filter[:minion] if filter[:minion]
    @notifications = @notifications.where notifiable_id: filter[:notifiable] if filter[:notifiable]
    @notifications = @notifications.where notifiable_type: filter[:notifiable_type] if filter[:notifiable_type]
    @notifications = @notifications.where 'notifications.text ILIKE ?', "%#{filter[:text]}%" if filter[:text]
    @notifications = @notifications.limit(params[:limit]) if params[:limit]
    @notifications = @notifications.offset(params[:offset]) if params[:offset]
    @notifications = @notifications.order 'run_at DESC'
    if filter[:pacient_name]
      @notifications = @notifications
      .joins("INNER JOIN pacients ON notifications.notifiable_id = pacients.id")
      .where 'pacients.name ILIKE ?', "%#{filter[:pacient_name]}%"
    end
    render json: @notifications
  end

  def create
    @notifications = params[:pacients].map do |pacient_id|
      pacient = Pacient.find pacient_id
      Notification.create! notifiable: pacient, text: params[:text], run_at: params[:run_at]
    end
    render json: @notifications
  end

  def destroy
    @notification = Notification.find(params[:id])
    if @notification.destroy
      render json: { id: @notification.id }
    else
      render json: { errors: @notification.errors.full_messages }, status: 422
    end
  end

  def message
    if @minion
      #@notifications = Notification.notify_sms.must_send.max_future_half_hour.limit(10).order 'run_at ASC'
      # Agora enviará apaenas status=pending para ver se impacta na repetição
      @notifications = Notification.notify_sms.pending.max_future_half_hour.limit(10).order 'run_at ASC'
      minion_id = @minion.id
      notifi_json = @notifications.as_json.map {|n| n['text'] = clean_charset(n['text']); n }
    elsif @pacient
      no_answer = Notification.monitorings[:no_answer]
      one_hous_ago = (Time.now-1*60*60).strftime('%F %H:%M UTC').to_time
      one_day = (Time.now+24*60*60).strftime('%F %H:%M UTC').to_time
      @notifications = @pacient.notifications
                               .where('run_at BETWEEN ? AND ?', one_hous_ago, one_day)
                               .where(monitoring: no_answer)
      notifi_json = @notifications.as_json
    else
      return render json: { ok: false }
    end
    #@notifications.update_all updated_at: Time.now.utc, status: :sending, minion_id: minion_id
    render json: { ok: true, messages: notifi_json }
    #@notifications.where(status: [:sending, :pending]).update_all status: :sending, minion_id: minion_id
    #@notifications.update_all status: :sending, minion_id: minion_id
  end

  def update_message
#    notification = Notification.find params[:id]
#    if params[:status]
#      notification.status = params[:status]
#      notification.fail_count += 1 if params[:status] == 'failed'
#      notification.minion_error = params[:error] if params[:error]
#    elsif params[:monitoring]
#      notification.monitoring = params[:monitoring]
#    else
#      return render json: { ok: false }
#    end
#    notification.save!
    # It looks like, in some cases, the record is not updated. So, we try do it with less abstraction.
    conn = ActiveRecord::Base.connection
    begin
      status = Notification.statuses[params[:status]]
      monitoring = Notification.monitorings[params[:monitoring]]
      id = params[:id].to_i
      return render json: { ok: false } unless id
      notification = Notification.find id
      columns = [ 'updated_at = %s' % conn.quote(Time.now.utc) ]
      if status
        columns << 'status = %s' % conn.quote(status)
        if params[:status] == 'failed'
          fail_count = notification.fail_count + 1
          columns << 'fail_count = %s' % fail_count
        end
        if params[:error]
          columns << 'minion_error = %s' % conn.quote(params[:error])
        end
      elsif monitoring
        columns << 'monitoring = %s' % conn.quote(monitoring)
      else
        return render json: { ok: false }
      end
      sql = "UPDATE notifications SET #{columns.join ', '} WHERE id = #{notification.id}"
      Rails.logger.debug "SQL: #{sql}"
      result = conn.execute sql
      if result.error_message.length > 0
        Rails.logger.error result.error_message
        return render json: { ok: false }
      else
        Rails.logger.info "Update with no fatality. #{result.res_status result.result_status}"
      end
      render json: { ok: true }
    rescue ActiveRecord::ActiveRecordError => err
      Rails.logger.error err
      render json: { ok: false }
    end
  end

  protected

  def clean_charset(str)
    str.gsub(/\bé\b/, 'eh')
    .gsub(/[áàãâ]/, 'a').gsub(/[ÁÀÃÂ]/, 'A')
    .gsub(/[éèêẽ]/, 'e').gsub(/[ÉÈÊẼ]/, 'e')
    .gsub(/[íìîĩ]/, 'i').gsub(/[ÍÌÎĨ]/, 'i')
    .gsub(/[óòôõ]/, 'o').gsub(/[ÓÒÔÕ]/, 'o')
    .gsub(/[úùûũ]/, 'u').gsub(/[ÚÙÛŨ]/, 'u')
    .gsub(/[ç]/, 'c').gsub(/[Ç]/, 'C')
  end

end
