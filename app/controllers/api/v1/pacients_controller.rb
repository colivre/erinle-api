class Api::V1::PacientsController < ApplicationController
  before_action :authenticate_collaborator

  def index
    columns = []
    values = []
    (params[:filter] || []).each do |key, val|
      columns << key.gsub(/[^a-z]/, '') + ' ILIKE ?'
      values << val
    end
    @pacients = Pacient.where(columns.join(' AND '), *values).order(:name)
    @pacients = @pacients.limit(params[:limit]) if params[:limit]
    @pacients = @pacients.offset(params[:offset]) if params[:offset]
    render json: @pacients
  end

  def autocomplete_data
    render json: Pacient.order(:name).all.to_json(:only => [ :id, :name, :nin ])
  end

  def show
    @pacient = Pacient.find(params[:id])
    render json: @pacient
  end

  def create
    @pacient = Pacient.create(pacient_params)
    if @pacient.persisted?
      render json: @pacient
    else
      render json: { errors: @pacient.errors.full_messages }, status: 422
    end
  end

  def update
    @pacient = Pacient.find(params[:id])
    if @pacient.update(pacient_params)
      render json: @pacient
    else
      render json: { errors: @pacient.errors.full_messages }, status: 422
    end
  end

  def destroy
    @pacient = Pacient.find(params[:id])
    if @pacient.destroy
      render json: @pacient
    else
      render json: { errors: @pacient.errors.full_messages }, status: 422
    end
  end

  def search
    @pacients = Pacient.search(params[:q])
  end

  private

  def pacient_params
    if params[:pacient][:picture] == 'undefined'
      Rails.logger.warn 'The client is sending "undefined" as picture value. JS Error?'
      params[:pacient].delete :picture
    end
    params.require(:pacient).permit(
      :name, :nin, :phone, :helth_id, :sex, :addr, :born,
      :picture, :notification_type, :app_key
    )
  end

end
