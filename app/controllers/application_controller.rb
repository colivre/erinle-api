class ApplicationController < ActionController::API

  include Knock::Authenticable

  before_action :set_paper_trail_whodunnit

  def user_for_paper_trail
    current_collaborator.try(:id)
  end
end
