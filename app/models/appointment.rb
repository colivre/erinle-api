class Appointment < ApplicationRecord
  has_paper_trail

  enum status: [ :pending, :suspended, :finished ]

  # -- Associations --

  belongs_to :pacient
  belongs_to :collaborator
  has_many :notifications, as: :notifiable, dependent: :delete_all

  # -- Validations --

  validates :run_at, presence: true
  validates :description, presence: true, length: { maximum: 100 }
  validates :notes, length: { maximum: 1000 }

  def pacient_pic
    pacient.picture.url(:thumb)
  end

  def collaborator_pic
    collaborator.picture.url(:thumb)
  end

  def as_json(options = nil)
    super(include: [:pacient, :collaborator], methods: [:pacient_pic, :collaborator_pic])
  end

  scope :should_notify_at?, ->(date) do
    throw "It is not UTC time (#{date})." unless is_utc date
    where(status: :pending).where(run_at: date.to_time)
  end

  def should_notify?
    status == 'pending'
  end

  def update_notification_pair
    one_hour = run_at - 60*60
    one_day = run_at - 24*60*60
    time = run_at.strftime '%H:%M'
    notifications.destroy_all
    if should_notify?
      [
        unsecure_create_notification(one_hour, "#{time} é hora da consulta."),
        unsecure_create_notification(one_day, "Amanhã às #{time} você tem uma consulta.")
      ]
    end
  end

  def unsecure_create_notification(date, ini_msg)
    throw "It is not UTC time (#{date})." unless is_utc date
    n = Notification.create(
      notifiable: self,
      run_at: date,
      text: "#{ini_msg}\n#{description}"
    )
    n.save!
    n
  end

  # Create notifications for all appointments in a time interval
  def self.create_notifications(tstart=nil, tend=nil)
    tstart ||= Time.now.strftime('%F %H:%M UTC').to_time
    tend ||= (Time.now+60*60*2).strftime('%F %H:%M UTC').to_time
    throw "It is not UTC time (#{tstart})." unless is_utc tstart
    throw "It is not UTC time (#{tend})." unless is_utc tend
    counter = 0
    where('run_at BETWEEN ? AND ?', tstart, tend).find_each do |appointment|
      counter += 2 if appointment.create_notification_pair
    end
    puts ">> #{counter} appointment notifications was created." unless Rails.env == 'test'
  end

  def self.create_notifications_to_next_hours(hours=5)
    create_notifications nil, (Time.now+60*60*hours).strftime('%F %H:%M UTC').to_time
  end

  protected

  def self.is_utc(t)
    t.utc? || t.zone == nil || t.zone == '00'
  end

  def is_utc(t)
    self.class.is_utc t
  end

end
