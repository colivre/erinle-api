class Collaborator < ApplicationRecord
  has_secure_password

  has_paper_trail
  has_attached_file :picture, styles: { medium: "400x400>", thumb: "200x200>" }, default_url: "/assets/:style/missing-person.png"

  # -- Associations --

  has_many :appointments, dependent: :destroy
  has_many :pacients, through: :appointments

  # -- Scope --

  scope :enabled, -> { where(enabled: true) }

  # -- Validations --

  validates :name, presence: true, length: { maximum: 50 }
  validates :username, presence: true, length: { maximum: 30 }, uniqueness: { message: 'is being used by another collaborator' }
  validates :nin, presence: true, format: { with: /\A\d{3}\.\d{3}\.\d{3}-\d{2}\z/, message: 'uses wrong format' }, uniqueness: { message: 'is being used by another collaborator' }
  validates :information, length: { maximum: 1000 }
  validates_attachment_content_type :picture, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  # -- Class methods --

  def self.from_token_request(request)
    # Returns a valid user, `nil` or raise `Knock.not_found_exception_class_name`
    login = request.params["auth"] && request.params["auth"]["username"]
    self.enabled.find_by username: login
  end

  # -- Instance methods --

  def to_token_payload
    JSON.parse(self.to_json).merge sub: id
  end

  def pic
    {
      thumb: picture.url(:thumb),
      medium: picture.url(:medium),
      original: picture.url
    }
  end

  def as_json(options = {})
    super(options.reverse_merge(methods: :pic, except: :password_digest))
  end

end
