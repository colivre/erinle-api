class Dosage < ApplicationRecord
  has_paper_trail

  enum status: [ :in_treatment, :interrupted, :suspended, :finished ]

  # -- Associations --

  belongs_to :drug
  belongs_to :pacient
  has_many :notifications, as: :notifiable, dependent: :delete_all

  # -- Validations --

  validates :start_at, :amount, :amount_unit, :cron_minute, presence: true
  validates :notes, length: { maximum: 1000 }
  validate do |dose|
    dose.amount_unit.strip!
    dose.amount = dose.amount.to_f
    unless is_utc start_at
      errors.add :base, "\"Start at\" is not UTC time (#{start_at})."
    end
    unless start_at.min%5 == 0
      errors.add :base, "\"Start at\" minute must be divisible by 5."
    end
    unless end_at.blank? || is_utc(end_at)
      errors.add :base, "\"End at\" is not UTC time (#{end_at})."
    end
    if end_at.blank? && total_dosages < 1
      errors.add :base, "You must set some limit: \"End at\" or \"total dosages\"."
    end
    unless dose.cron_minute
      errors.add :base, "Minute can't be \"all\"."
    end
    dose.cron_minute.each do |min|
      if (min > 0 && (min%5) > 0)
        errors.add :base, "Minute must be divisible by 5."
      end
    end
    if dose.cron_month
      dose.cron_month.each do |month|
        if (month < 0)
          errors.add :base, "Month can't be a interval."
        end
      end
    end
    if dose.cron_w_day
      dose.cron_w_day.each do |month|
        if (month < 0)
          errors.add :base, "Weekday can't be a interval."
        end
      end
    end
    other = Dosage.where(pacient_id: dose.pacient_id, drug_id: dose.drug_id, amount: dose.amount, amount_unit: dose.amount_unit, start_at: dose.start_at).first
    if other && other.id != dose.id
      errors.add :base, "#{dose.pacient.name} já tem essa dose cadastrada."
    end
  end

  # -- Scope --

  ActiveRecord::Base.connection.execute "
    CREATE OR REPLACE FUNCTION is_time_interval(start_date timestamp, test_date timestamp, t_unit text, rule_list int[])
    RETURNS boolean AS $$
    DECLARE
      val int;
      d_unit int;
      delta interval := test_date - start_date;
      epoch_delta int := date_part('EPOCH', delta);
      d_minutes int := epoch_delta / 60;
      d_hours int := d_minutes / 60;
      d_days int := d_hours / 24;
    BEGIN
      IF t_unit = 'MIN'   THEN d_unit := d_minutes; END IF;
      IF t_unit = 'HOURS' THEN d_unit := d_hours;   END IF;
      IF t_unit = 'DAYS'  THEN d_unit := d_days;    END IF;
      RAISE NOTICE 'Delta %: %', t_unit, d_unit;
      IF rule_list IS NOT NULL THEN
        FOREACH val IN ARRAY rule_list LOOP
          IF val < 0 THEN RAISE NOTICE 'val % (mod: %)', val, (d_unit % val); END IF;
          IF val < 0 AND (d_unit % val) = 0 THEN
            RAISE NOTICE 'Pegou';
            RETURN true;
          END IF;
        END LOOP;
      END IF;
      RETURN false;
    END;
    $$ LANGUAGE plpgsql;
  "

  scope :should_notify_at?, ->(date) do
    date     = date.to_time
    throw "It is not UTC time (#{date})." unless is_utc date
    str_date = date.strftime '%F %T'
    # `unit IS NULL` test for cron `*`, that means `all`
    # `array_positions(unit, #{date.unit})` filter the array for this unit,
    #   that means it will find this specific time unit value, if is setup.
    #   Like to match hour `8` in cron hour list `4,8,12,16`.
    # `is_time_interval()` test for cron `*/#` notation, however anchored to
    #   some date-time. It allows the `*/4` days notation, starting 30/jan to
    #   repeat at 3/feb.
    where(status: :in_treatment).
    where "
      ( start_at <= timestamp '#{str_date}' ) AND
      ( end_at IS NULL OR end_at >= timestamp '#{str_date}' ) AND
      (
        (cron_month IS NULL) OR
        (cron_month = ARRAY[]::int[]) OR
        (array_length(array_positions(cron_month, #{date.month}),1) > 0)
        -- cron_month must not be an interval
      ) AND (
        (cron_w_day IS NULL) OR
        (cron_w_day = ARRAY[]::int[]) OR
        (array_length(array_positions(cron_w_day, #{date.wday}),1) > 0)
        -- cron_w_day must not be an interval
      ) AND (
        (cron_m_day IS NULL) OR
        (cron_m_day = ARRAY[]::int[]) OR
        (array_length(array_positions(cron_m_day, #{date.day}),1) > 0) OR
        is_time_interval(start_at, timestamp '#{str_date}', 'DAYS', cron_m_day)
      ) AND (
        (cron_hour IS NULL) OR
        (cron_hour = ARRAY[]::int[]) OR
        (array_length(array_positions(cron_hour, #{date.hour}),1) > 0) OR
        is_time_interval(start_at, timestamp '#{str_date}', 'HOURS', cron_hour)
      ) AND (
        -- cron_minute must not be NULL
        (array_length(array_positions(cron_minute, #{date.min}),1) > 0) OR
        is_time_interval(start_at, timestamp '#{str_date}', 'MIN', cron_minute)
      )
    "
  end

  # -- Instance Methods --

  def pacient_pic
    pacient.picture.url(:thumb)
  end

  def drug_img
    drug.image.url(:thumb)
  end

  def notif_info
    {
      pending: notifications.pending.length,
      sending: notifications.sending.length,
      completed: notifications.completed.length,
      failed: notifications.failed.length
    }
  end

  def as_json(options = nil)
    super(include: [:drug, :pacient], methods: [:pacient_pic, :drug_img, :notif_info])
  end

  def should_notify_at?(date=Time.now.utc)
    !!Dosage.where('id = ?', id).should_notify_at?(date).first
  end

  # Test if it should notify at timestamp argument...
  # - if not, return nil,
  # - if yes, test if a notification to that time exists...
  #   - if yes, just return that,
  #   - if not, create a notification and return it.
  def create_notification_at(date)
    if should_notify_at? date
      n = notifications.run_at(date).first
      return n if n
      unsecure_create_notification_at date
    end
  end

  def unsecure_create_notification_at(date)
    throw "It is not UTC time (#{date})." unless is_utc date
    n = Notification.create(
      notifiable: self,
      run_at: date,
      text: "#{date.strftime '%H:%M'} é hora de #{amount} #{amount_unit} de #{drug.name}\n#{description}"
    )
    n.save!
    # total_dosages == 0 means no limit.
    if total_dosages > 0
      finished! if notifications.count >= total_dosages
    end
    n
  end

  # Create notifications for this dose in a time interval
  def create_notifications(tstart=nil, tend=nil)
    tstart ||= Time.now.strftime('%F %H:%M UTC').to_time
    tend ||= (Time.now+60*60*2).strftime('%F %H:%M UTC').to_time
    tstart = tstart.strftime('%%F %%H:%02d UTC' % (tstart.min-tstart.min%5)).to_time
    t = tstart
    five_minutes = 60*5
    counter = 0
    while t <= tend
      counter += 1 if create_notification_at(t)
      t += five_minutes
    end
    counter # notifications created.
  end

  # Create notifications for all doses in a time interval
  def self.create_notifications(tstart=nil, tend=nil)
    tstart ||= Time.now.strftime('%F %H:%M UTC').to_time
    tend ||= (Time.now+60*60*2).strftime('%F %H:%M UTC').to_time
    tstart = tstart.strftime('%%F %%H:%02d UTC' % (tstart.min-tstart.min%5)).to_time
    t = tstart
    five_minutes = 60*5
    counter = 0
    while t <= tend
      should_notify_at?(t).find_each do |dose|
        if dose.notifications.where(run_at: t).empty?
          dose.unsecure_create_notification_at t
          counter += 1
        end
      end
      t += five_minutes
    end
    finish_notifications_after_end_at
    puts ">> #{counter} dosage notifications was created." unless Rails.env == 'test'
  end

  def self.finish_notifications_after_end_at
    where("end_at < ?", Time.now.strftime('%F %H:%M UTC')).update_all status: :finished
  end

  def self.create_notifications_to_next_hours(hours=5)
    create_notifications nil, (Time.now+60*60*hours).strftime('%F %H:%M UTC').to_time
  end

  protected

  def self.is_utc(t)
    t.utc? || t.zone == nil || t.zone == '00'
  end

  def is_utc(t)
    self.class.is_utc t
  end

end
