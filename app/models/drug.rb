class Drug < ApplicationRecord
  has_paper_trail
  has_attached_file :image, styles: { medium: "400x400>", thumb: "200x200>" }, default_url: "/assets/:style/missing-drug.png"

  # -- Associations --

  has_many :dosages, dependent: :destroy

  # -- Validations --

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  validates :name, presence: true, length: { maximum: 255 }
  validates :concentration, presence: true
  validates :form, presence: true
  validates :description, presence: true, length: { maximum: 5000 }
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  # -- Scopes --

  scope :search, -> (query) { where('name LIKE ?', "%#{query}%") }

  # -- Instance methods --

  def img
    {
      thumb: image.url(:thumb),
      medium: image.url(:medium),
      original: image.url
    }
  end

  def as_json(options = {})
    super(options.reverse_merge(methods: :img))
  end

end
