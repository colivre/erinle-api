class Environment < ApplicationRecord
  has_attached_file :logo, styles: { medium: "500x500#", thumb: "125x125#" }, default_url: "/assets/:style/logo.png"
  has_attached_file :favicon, default_url: "/assets/original/logo.ico"

  # -- Validations --

  validates :name, presence: true, length: { maximum: 30 }
  validates_attachment_content_type :logo, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  # -- Instance methods --

  def logo_url
    logo.url
  end

  def favicon_url
    favicon.url
  end

end
