class Minion < ApplicationRecord

  # -- Scope --

  scope :enabled, -> { where(enabled: true) }

end
