class Notification < ApplicationRecord

  require 'cgi'

  enum status: [:pending, :sending, :completed, :failed] # The message was delivered to the pacient?
  enum monitoring: [:no_answer, :done, :not_done] # The pacient did what the notification request?
  # If it is a dosage notification, "done" means the paciente took the medication.

  class << self

    def collect_notifications_to_send_now
      notifications = Notification.notify_sms.must_send
      Rails.logger.debug "==> Notifications to send now by SMS service: #{notifications.length}"
      Rails.logger.debug(notifications.map {|n| "#{n.id} #{n.status} #{n.pacient.phone} #{n.text}" }.join("\n"))
      notifications.update_all updated_at: Time.now.utc, status: :sending
      notifications
    end

    ### Send via twilio.com ###

    def twilio
      begin
        return @@twilio ||= YAML.load_file('config/twilio.yml')
      rescue Errno::ENOENT
        Rails.logger.debug 'Não tem twilio.yml!'
        return
      end
    end

    def send_twilio_notifications
      return if !twilio.present?
      Rails.logger.debug "Send with Twilio"
      collect_notifications_to_send_now.each do |n|
        phone = '+550' + n.pacient.phone.gsub(/[^0-9]/, '')
        result = send_sms_twilio(phone, n.text)
        if result[:error][:code] > 0
          Rails.logger.debug "==> Twilio falhou #{phone} #{result[:error][:message]}"
          n.status = :failed
          n.fail_count += 1
          n.minion_error = result[:error][:message]
        else
          n.status = :completed
          Rails.logger.debug "==> Twilio Sucesso #{phone}!"
        end
        begin
          n.save!
        rescue Exception => error
          Rails.logger.error "==> Fail to save notification #{phone} -- #{n.text} -- #{error.message}"
          Rails.logger.error e.backtrace.join("\n")
        end
        n.reload
        Rails.logger.debug "==> New(?) status for #{phone}: #{n.status}"
        Rails.logger.debug "==> notification finalizada: #{n.inspect}"
      end
    end

    def send_sms_twilio(phone, message)
      Rails.logger.debug "==> Send SMS via Twilio to #{phone}"
      #client = Twilio::REST::Client.new(twilio['account_sid'], twilio['auth_token'])
      #client.http_client.timeout = 10
      #client.messages.create from: twilio['twilio_num'], to: phone, body: message
      url = "https://api.twilio.com/2010-04-01/Accounts/#{twilio['account_sid']}/Messages.json"
      out = IO.popen(['curl', url, '-X', 'POST',
        '--data-urlencode', "To=#{phone}",
        '--data-urlencode', "From=#{twilio['twilio_num']}",
        '--data-urlencode', "Body=#{message}",
        '-u', twilio['account_sid']+':'+twilio['auth_token']
        ]).read
      Rails.logger.debug "==> Twilio response: #{out}"
      result = JSON.parse out
      result[:error] = {
        code: (result['error_code'] || result['code'] || 0).to_i,
        message: result['error_message'] || result['message'] || ''
      }
      result
    end

    ### Send via Marktel.com.br ###

    def marktel
      begin
        return @@marktel ||= YAML.load_file('config/marktel.yml')
      rescue Errno::ENOENT
        Rails.logger.debug 'Não tem marktel.yml!'
        return
      end
    end

    def send_marktel_notifications
      return if !marktel.present?
      Rails.logger.debug "Send with Marktel"
      collect_notifications_to_send_now.each do |n|
        phone = n.pacient.phone.gsub(/[^0-9]/, '')
        result = send_sms_marktel(phone, n.text)
        if result['error_string']
          Rails.logger.debug "==> Marktel falhou #{phone} #{result['error_string']}"
          n.status = :failed
          n.fail_count += 1
          n.minion_error = result['error_string']
        else
          n.status = :completed
          Rails.logger.debug "==> Marktel Sucesso #{phone}!"
        end
        begin
          n.save!
        rescue Exception => error
          Rails.logger.error "==> Fail to save notification #{phone} -- #{n.text} -- #{error.message}"
          Rails.logger.error e.backtrace.join("\n")
        end
        n.reload
        Rails.logger.debug "==> New(?) status for #{phone}: #{n.status}"
        Rails.logger.debug "==> notification finalizada: #{n.inspect}"
      end
    end

    def send_sms_marktel(phone, message)
      Rails.logger.debug "==> Send SMS via Marktel to #{phone}"
      url = "http://sms.painelmarktel.com.br/index.php?app=ws" +
            "&u=#{marktel['user']}&h=#{marktel['token']}&op=pv&to=#{phone}" +
            "&msg=#{CGI.escape message}&format=json"
      out = IO.popen(['curl', url]).read
      Rails.logger.debug "==> Marktel response: #{out}"
      result = JSON.parse out
      # {"status":"ERR","error":"100","error_string":"autenticacao falha","timestamp":1567192703}
      # {"data":[{"status":"OK","error":"0","smslog_id":"11","queue":"22","to":"33"}],"error_string":null,"timestamp":44}
      data = (result['data'] && result['data'][0]) \
        ? result['data'][0] \
        : {'error'=>'999', 'error_string'=>'No data'}
      data['error_string'] ||= "Error #{data['error']}" unless data['error'] == '0'
      result['error_string'] ||= data['error_string']
      result
    end

  end

  # -- Associations --

  belongs_to :notifiable, polymorphic: true
  belongs_to :minion, required: false

  # -- Validations --

  validate do
    unless is_utc run_at
      errors.add :base, "It is not UTC time (#{run_at})."
    end
  end

  # -- Scope --

  scope :must_send, -> do
    pending = statuses[:pending]
    sending = statuses[:sending]
    failed  = statuses[:failed]
    six_min_ago = Time.now - 6.minutes # must be host's local time, because of Rails

    where('
      status = ?
      OR (status = ? AND fail_count <= 5 AND updated_at <= ?)
      OR (status = ? AND updated_at <= ?)
    ', pending, failed, six_min_ago, sending, six_min_ago).
    where('run_at >= ?', (Time.now - 20.minutes).strftime('%F %H:%M UTC').to_time).
    where('run_at <= ?', (Time.now + 1.minute).strftime('%F %H:%M UTC').to_time).
    order('run_at ASC')
  end

  scope :run_at, ->(time) do
    throw 'It is not UTC time.' unless is_utc time
    where(run_at: time)
  end

  scope :max_future_half_hour, -> do
    # must be timezone less
    where 'run_at <= ?', (Time.now+30*60).strftime('%F %H:%M UTC').to_time
  end

  def self.notify_sms
    nt_sms = Pacient.notification_types[:sms]
    g1 = self
    .joins("INNER JOIN dosages ON notifications.notifiable_id = dosages.id")
    .joins("INNER JOIN pacients ON dosages.pacient_id = pacients.id")
    .where("notifications.notifiable_type = 'Dosage'")
    .where("pacients.notification_type = #{nt_sms}")
    g2 = self
    .joins("INNER JOIN appointments ON notifications.notifiable_id = appointments.id")
    .joins("INNER JOIN pacients ON appointments.pacient_id = pacients.id")
    .where("notifications.notifiable_type = 'Appointment'")
    .where("pacients.notification_type = #{nt_sms}")
    g3 = self
    .joins("INNER JOIN pacients ON notifications.notifiable_id = pacients.id")
    .where("notifications.notifiable_type = 'Pacient'")
    .where("pacients.notification_type = #{nt_sms}")
    self
    .from("((#{g1.to_sql}) UNION (#{g2.to_sql}) UNION (#{g3.to_sql})) AS notifications")
  end

  ['dosage', 'appointment', 'pacient'].each do |key|
    define_method("#{key}?") { notifiable.is_a? key.capitalize.constantize }
  end

  def pacient
    pacient? ? notifiable : notifiable.pacient
  end

  def description
    pacient? ? text : notifiable.description
  end

  def dosage_json
    if dosage?
      {
        drug_img: notifiable.drug_img,
        drug_name: notifiable.drug.name,
        amount: notifiable.amount,
        amount_unit: notifiable.amount_unit,
        description: notifiable.description
      }
    end
  end

  def as_json(options = {})
    super(options).merge(
      run_at: run_at.strftime('%FT%T'),
      phone: pacient.phone,
      dosage: dosage_json,
      pacient: {
        id: pacient.id,
        name: pacient.name,
        nin: pacient.nin,
        pic: {thumb: pacient.picture.url(:thumb)}
      }
    )
  end

  protected

  def self.is_utc(t)
    t.utc? || t.zone == nil || t.zone == '00'
  end

  def is_utc(t)
    self.class.is_utc t
  end

end
