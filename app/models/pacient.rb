class Pacient < ApplicationRecord
  has_paper_trail
  has_attached_file :picture, styles: { medium: "400x400>", thumb: "200x200>" }, default_url: "/assets/:style/missing-person.png"

  enum notification_type: [ :sms, :app ]
  enum sex: [ :undeclared, :male, :female ]

  # -- Associations --

  has_many :appointments, dependent: :destroy
  has_many :collaborators, through: :appointments
  has_many :dosages, dependent: :destroy
  has_many :personal_notifications, as: :notifiable, class_name: 'Notification', dependent: :destroy
  has_many :dosage_notifications, through: :dosages, as: :notifiable, source: :notifications
  has_many :appointment_notifications, through: :appointments, as: :notifiable, source: :notifications

  # -- Validations --

  validates :name, presence: true, length: { maximum: 50 }
  validates :nin, presence: true, format: { with: /\A\d{3}\.\d{3}\.\d{3}-\d{2}\z/, message: 'uses wrong format' }, uniqueness: { message: 'is being used by another pacient' }
  validates :phone, presence: true
  validates :notification_type, presence: true
  validates_attachment_content_type :picture, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  # -- Scopes --

  scope :search, -> (query) { where('name LIKE ?', "%#{query}%") }

  def notifications
    Notification.from("
      (
        (#{personal_notifications.to_sql})
        UNION (#{dosage_notifications.to_sql})
        UNION (#{appointment_notifications.to_sql})
      ) AS notifications
    ")
  end

  # -- Callbacks --

  after_create :generate_app_key

  # -- Instance methods --

  def pic
    {
      thumb: picture.url(:thumb),
      medium: picture.url(:medium),
      original: picture.url
    }
  end

  def as_json(options = {})
    super(options.reverse_merge(methods: :pic))
  end

  private

  def generate_app_key
    #TODO
  end

end
