json.ok true

json.array! @notifications do |notification|
  json.id notification.id
  json.phone notification.pacient.phone
  json.description notification.description
  if notification.dosage?
    json.amount notification.notifiable.amount
    json.drug do
      json.name notification.notifiable.drug.name
      json.image_url notification.notifiable.drug.image.url
    end
  end
end
