# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

if Rails.env != 'test'
  begin
    if Collaborator.where(administrator: true).empty? && $0 !~ /scripts\/create-adm/
      cmd = "RAILS_ENV=#{Rails.env} ./scripts/create-adm"
      cmd += ' ' while cmd.length < 45
      # 00        10        20        30        40
      # 123456789|123456789|123456789|123456789|123456789|
      puts "
        ╔════════════════════════════════════════════════╗
        ║                ⚠  ATENTION!  ⚠                 ║
        ╟────────────────────────────────────────────────╢
        ║  You have no registered admin!                 ║
        ║  Please run:                                   ║
        ║  #{cmd} ║
        ╚════════════════════════════════════════════════╝
      "
    end
  rescue ActiveRecord::StatementInvalid => e
    puts "There is no collaborators table yet."
  end
end
