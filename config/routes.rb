Rails.application.routes.draw do
  namespace :api, constraints: -> (req) { req.format == :json } do
    namespace :v1 do
      post 'login' => 'collaborator_token#create'
      resources :drugs do
        get 'search'
        get 'autocomplete_data', on: :collection
      end
      resources :collaborators do
        get 'search'
        get 'autocomplete_data', on: :collection
      end
      resources :pacients do
        get 'search'
        get 'autocomplete_data', on: :collection
      end
      resources :appointments
      resources :dosages
      resources :minions
      resources :notifications
      resources :environments, only: :index
      resources :changes, only: [:index, :show] do
        post 'search', on: :collection
      end
      get :message, to: 'notifications#message'
      #get 'message/:id', to: 'notifications#update_message' # not working :-(
    end
  end
  # Respond with not found any route that didn't match above
  match '/', to: -> (env) { [200, {}, ['Welcome to Erinle API Server']] }, via: :all
  get '/api/v1/message/:id', to: 'api/v1/notifications#update_message'
  get '/api/v1/summary', to: 'api/v1/environments#summary'
  match '*path', to: -> (env) {
    Rails.logger.debug "Completed 404 (generic match)"
    [404, {}, ['{"error": "not_found"}']]
  }, via: :all
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
