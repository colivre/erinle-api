class CreatePacients < ActiveRecord::Migration[5.1]
  def change
    create_table :pacients do |t|
      t.string :first_name
      t.string :last_name
      t.string :nin
      t.string :phone
      t.string :app_key

      t.timestamps
    end
  end
end
