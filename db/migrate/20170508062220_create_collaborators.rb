class CreateCollaborators < ActiveRecord::Migration[5.1]
  def change
    create_table :collaborators do |t|
      t.string :first_name
      t.string :last_name
      t.string :nin
      t.string :username
      t.string :password
      t.string :information, limit: 1000
      t.boolean :administrator, default: false, null: false
      t.boolean :enabled, default: true, null: false

      t.timestamps
    end
  end
end
