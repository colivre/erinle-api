class CreateAppointments < ActiveRecord::Migration[5.1]
  def change
    create_table :appointments do |t|
      t.datetime :run_at
      t.integer :notification_type, default: 0
      t.integer :status, default: 0
      t.string :description
      t.string :notes, limit: 1000

      t.references :collaborator, index: true
      t.references :pacient, index: true

      t.timestamps
    end
  end
end
