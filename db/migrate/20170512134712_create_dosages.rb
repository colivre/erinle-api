class CreateDosages < ActiveRecord::Migration[5.1]
  def change
    create_table :dosages do |t|
      t.datetime :start_at
      t.integer :amount
      t.integer :interval_in_minutes
      t.integer :notification_type, default: 0
      t.integer :notification_rate, default: 0
      t.integer :status, default: 0
      t.string :description
      t.string :notes, limit: 1000

      t.references :drug, index: true
      t.references :pacient, index: true

      t.timestamps
    end
  end
end
