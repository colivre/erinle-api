class CreateEnvironments < ActiveRecord::Migration[5.1]
  def change
    create_table :environments do |t|
      t.string :name
      t.boolean :default, default: false, null: false

      t.timestamps
    end

    add_reference :collaborators, :environment, index: true
    add_foreign_key :collaborators, :environments

    add_reference :pacients, :environment, index: true
    add_foreign_key :pacients, :environments

    add_reference :drugs, :environment, index: true
    add_foreign_key :drugs, :environments
  end
end
