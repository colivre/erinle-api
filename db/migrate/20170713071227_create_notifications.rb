class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.datetime :run_at
      t.integer :notifiable_id
      t.string  :notifiable_type
      t.integer :status, default: 0

      t.timestamps
    end

    add_index :notifications, [:notifiable_type, :notifiable_id]

  end
end
