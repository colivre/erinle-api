class ReplaceFirstAndLastNameWithName < ActiveRecord::Migration[5.1]
  def change
    remove_column :collaborators, :last_name, :string
    rename_column :collaborators, :first_name, :name

    remove_column :pacients, :last_name, :string
    rename_column :pacients, :first_name, :name
  end
end
