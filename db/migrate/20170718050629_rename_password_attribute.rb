class RenamePasswordAttribute < ActiveRecord::Migration[5.1]
  def change
    rename_column :collaborators, :password, :password_digest
  end
end
