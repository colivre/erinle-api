class RemoveMultipleEnvironments < ActiveRecord::Migration[5.1]
  def change
    remove_column :environments, :default

    remove_index :collaborators, :environment_id
    remove_index :drugs, :environment_id
    remove_index :pacients, :environment_id
    remove_column :collaborators, :environment_id
    remove_column :drugs, :environment_id
    remove_column :pacients, :environment_id
  end
end
