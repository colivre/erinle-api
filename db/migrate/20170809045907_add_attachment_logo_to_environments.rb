class AddAttachmentLogoToEnvironments < ActiveRecord::Migration[5.1]
  def self.up
    change_table :environments do |t|
      t.attachment :logo
    end
  end

  def self.down
    remove_attachment :environments, :logo
  end
end
