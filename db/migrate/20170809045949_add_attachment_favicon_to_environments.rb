class AddAttachmentFaviconToEnvironments < ActiveRecord::Migration[5.1]
  def self.up
    change_table :environments do |t|
      t.attachment :favicon
    end
  end

  def self.down
    remove_attachment :environments, :favicon
  end
end
