class AddAttachmentPictureToPacients < ActiveRecord::Migration[5.1]
  def self.up
    change_table :pacients do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :pacients, :picture
  end
end
