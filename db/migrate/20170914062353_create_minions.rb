class CreateMinions < ActiveRecord::Migration[5.1]
  def change
    create_table :minions do |t|
      t.string :key
      t.string :description
      t.boolean :enabled, default: true, null: false

      t.timestamps
    end
  end
end
