class AddAttachmentPictureToCollaborators < ActiveRecord::Migration[5.1]
  def self.up
    change_table :collaborators do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :collaborators, :picture
  end
end
