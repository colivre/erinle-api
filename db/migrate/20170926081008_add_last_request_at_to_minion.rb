class AddLastRequestAtToMinion < ActiveRecord::Migration[5.1]
  def change
    add_column :minions, :last_request_at, :datetime
  end
end
