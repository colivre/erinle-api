class CronLikeDosages < ActiveRecord::Migration[5.1]
  def change
    remove_column :dosages, :interval_in_minutes

    add_column :dosages, :cron_minute, :integer, array: true
    add_column :dosages, :cron_hour,   :integer, array: true
    add_column :dosages, :cron_m_day,  :integer, array: true
    add_column :dosages, :cron_month,  :integer, array: true
    add_column :dosages, :cron_w_day,  :integer, array: true
  end
end
