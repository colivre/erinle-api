class NotificationHasWonItsText < ActiveRecord::Migration[5.1]
  def change

    add_column :notifications, :text, :string

    add_belongs_to :notifications, :minion, foreign_key: true
    add_column :notifications, :minion_error, :string

  end
end
