class DosagesLimit < ActiveRecord::Migration[5.1]
  def change
    add_column :dosages, :end_at, :datetime
    add_column :dosages, :total_dosages, :integer, null: false, default: 0
  end
end
