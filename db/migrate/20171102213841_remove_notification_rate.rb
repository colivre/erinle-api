class RemoveNotificationRate < ActiveRecord::Migration[5.1]
  def change
    remove_column :dosages, :notification_rate
  end
end
