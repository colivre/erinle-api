class AddNotificationTypeToPacient < ActiveRecord::Migration[5.1]
  def change
    add_column :pacients, :notification_type, :integer, null: false, default: 0
  end
end
