class UpdateAppointments < ActiveRecord::Migration[5.1]
  def change
    remove_column :appointments, :notification_type
  end
end
