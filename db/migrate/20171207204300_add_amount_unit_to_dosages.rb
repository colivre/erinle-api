class AddAmountUnitToDosages < ActiveRecord::Migration[5.1]
  def change
    add_column :dosages, :amount_unit, :string, null: false, default: 'unidade'
  end
end
