class AddNewFieldsToPacient < ActiveRecord::Migration[5.1]
  def change
    add_column :pacients, :addr, :string, null: true
    add_column :pacients, :helth_id, :string, null: true
    add_column :pacients, :born, :datetime, null: true
    add_column :pacients, :sex, :integer, null: true
  end
end
