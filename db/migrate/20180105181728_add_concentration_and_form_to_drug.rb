class AddConcentrationAndFormToDrug < ActiveRecord::Migration[5.1]
  def change
    add_column :drugs, :concentration, :string, null: true
    add_column :drugs, :form, :string, null: true
  end
end
