class AddFailCounterToNotification < ActiveRecord::Migration[5.1]
  def change
    add_column :notifications, :fail_count, :integer, null: true, default: 0
  end
end
