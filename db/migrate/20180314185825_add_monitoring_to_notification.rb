class AddMonitoringToNotification < ActiveRecord::Migration[5.1]
  def change
    add_column :notifications, :monitoring, :integer, default: 0
  end
end
