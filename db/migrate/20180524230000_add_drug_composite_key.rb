class AddDrugCompositeKey < ActiveRecord::Migration[5.1]
  def change
    add_index :drugs, [:name, :concentration, :form], unique: true
  end
end
