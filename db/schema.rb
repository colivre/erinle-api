# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180524230000) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "appointments", force: :cascade do |t|
    t.datetime "run_at"
    t.integer "status", default: 0
    t.string "description"
    t.string "notes", limit: 1000
    t.bigint "collaborator_id"
    t.bigint "pacient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["collaborator_id"], name: "index_appointments_on_collaborator_id"
    t.index ["pacient_id"], name: "index_appointments_on_pacient_id"
  end

  create_table "collaborators", force: :cascade do |t|
    t.string "name"
    t.string "nin"
    t.string "username"
    t.string "password_digest"
    t.string "information", limit: 1000
    t.boolean "administrator", default: false, null: false
    t.boolean "enabled", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "picture_file_name"
    t.string "picture_content_type"
    t.integer "picture_file_size"
    t.datetime "picture_updated_at"
  end

  create_table "dosages", force: :cascade do |t|
    t.datetime "start_at"
    t.integer "amount"
    t.integer "notification_type", default: 0
    t.integer "status", default: 0
    t.string "description"
    t.string "notes", limit: 1000
    t.bigint "drug_id"
    t.bigint "pacient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cron_minute", array: true
    t.integer "cron_hour", array: true
    t.integer "cron_m_day", array: true
    t.integer "cron_month", array: true
    t.integer "cron_w_day", array: true
    t.datetime "end_at"
    t.integer "total_dosages", default: 0, null: false
    t.string "amount_unit", default: "unidade", null: false
    t.index ["drug_id"], name: "index_dosages_on_drug_id"
    t.index ["pacient_id"], name: "index_dosages_on_pacient_id"
  end

  create_table "drugs", force: :cascade do |t|
    t.string "name"
    t.string "description", limit: 5000
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.string "concentration"
    t.string "form"
    t.index ["name", "concentration", "form"], name: "index_drugs_on_name_and_concentration_and_form", unique: true
  end

  create_table "environments", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "logo_file_name"
    t.string "logo_content_type"
    t.integer "logo_file_size"
    t.datetime "logo_updated_at"
    t.string "favicon_file_name"
    t.string "favicon_content_type"
    t.integer "favicon_file_size"
    t.datetime "favicon_updated_at"
  end

  create_table "minions", force: :cascade do |t|
    t.string "key"
    t.string "description"
    t.boolean "enabled", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "last_request_at"
  end

  create_table "notifications", force: :cascade do |t|
    t.datetime "run_at"
    t.integer "notifiable_id"
    t.string "notifiable_type"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "text"
    t.bigint "minion_id"
    t.string "minion_error"
    t.integer "fail_count", default: 0
    t.integer "monitoring", default: 0
    t.index ["minion_id"], name: "index_notifications_on_minion_id"
    t.index ["notifiable_type", "notifiable_id"], name: "index_notifications_on_notifiable_type_and_notifiable_id"
  end

  create_table "pacients", force: :cascade do |t|
    t.string "name"
    t.string "nin"
    t.string "phone"
    t.string "app_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "picture_file_name"
    t.string "picture_content_type"
    t.integer "picture_file_size"
    t.datetime "picture_updated_at"
    t.integer "notification_type", default: 0, null: false
    t.string "addr"
    t.string "helth_id"
    t.datetime "born"
    t.integer "sex"
  end

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id"
    t.string "foreign_key_name", null: false
    t.integer "foreign_key_id"
    t.index ["foreign_key_name", "foreign_key_id"], name: "index_version_associations_on_foreign_key"
    t.index ["version_id"], name: "index_version_associations_on_version_id"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.integer "transaction_id"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
    t.index ["transaction_id"], name: "index_versions_on_transaction_id"
  end

  add_foreign_key "notifications", "minions"
end
