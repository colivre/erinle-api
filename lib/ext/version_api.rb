module VersionApi

  # -- Instance methods --

  def collaborator
    if whodunnit
      Collaborator.find_by id: whodunnit
    else
      'System'
    end
  end

  def formatted_changeset
    changeset.inject({}) do |h, changes|
      key = changes[0]
      before, after = *changes[1]

      translation = I18n.t(key, scope: "activerecord.attributes.#{item_type.downcase}", default: key.gsub(/_/, ' ').capitalize)
      if key.match(/password/)
        before = '☓☓☓☓☓' if before
        after = '☓☓☓☓☓' if after
      end
      h[translation] = [(before || ''), (after || '')] unless key.in? ['id', 'updated_at']

      h
    end
  end

  def as_json(options = {})
    super(options.reverse_merge(only: [:id, :item_type, :item_id, :event, :created_at], methods: [:collaborator, :formatted_changeset]))
  end

end

PaperTrail::Version.include VersionApi
