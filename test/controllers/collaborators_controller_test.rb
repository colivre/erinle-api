require 'test_helper'

class CollaboratorsControllerTest < ActionDispatch::IntegrationTest

  setup do
    @adm ||= Collaborator.create(
      name: 'Zé das Cove',
      username: 'ze',
      password: 'test',
      nin: '111.111.111-11',
      administrator: true
    )
    @adm.save!
    @usr ||= Collaborator.create(
      name: 'Fulano de Tal',
      username: 'fulano',
      password: 'test',
      nin: '222.222.222-22',
      administrator: false
    )
    @usr.save!
  end

  test "admins should see collaborator list" do
    get '/api/v1/collaborators', as: :json, headers: authenticated_header(@adm)
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal ['fulano', 'ze'], resp.map {|m| m['username']}.sort
  end

  test "simple user cant see collaborator list" do
    get '/api/v1/collaborators', as: :json, headers: authenticated_header(@usr)
    assert_response 401
    resp = JSON.parse @response.body
    assert_equal ['You are not an admin'], resp['errors']
  end

end
