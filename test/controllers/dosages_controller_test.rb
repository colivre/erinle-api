require 'test_helper'

class DosagesControllerTest < ActionDispatch::IntegrationTest

  setup do
    Dosage.all.map &:destroy
    Notification.all.map &:destroy
    @drug ||= Drug.create name: 'remedy', description: 'Nice remedy', concentration: '2mg', form: 'drops'
    @drug.save!
    @pacient ||= Pacient.create name: 'Zé', nin: '111.222.333-44', phone: '12-3456-7890'
    @pacient.save!
  end

  test "should list all dosages when no filtering" do
    pacient2 = Pacient.create name: 'Ada', nin: '999.999.999-99', phone: '11-1111-1111'
    create_dose [10], nil, nil, nil, nil, amount: 1
    create_dose [20], nil, nil, nil, nil, amount: 2, pacient: pacient2
    create_dose [30], nil, nil, nil, nil, amount: 3
    create_dose [40], nil, nil, nil, nil, amount: 4, pacient: pacient2
    assert_equal 4, Dosage.all.length
    get '/api/v1/dosages', as: :json, headers: authenticated_header
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal [[10],[20],[30],[40]], resp.map {|d| d['cron_minute']}.sort {|d1,d2| d1[0] <=> d2[0]}
  end

  test "can filter dosages list by pacient" do
    pacient2 = Pacient.create name: 'Ada', nin: '999.999.999-99', phone: '11-1111-1111'
    create_dose [10], nil, nil, nil, nil, amount: 1
    create_dose [20], nil, nil, nil, nil, amount: 2, pacient: pacient2
    create_dose [30], nil, nil, nil, nil, amount: 3
    create_dose [40], nil, nil, nil, nil, amount: 4, pacient: pacient2
    assert_equal 4, Dosage.all.length
    get "/api/v1/dosages?pacient=#{pacient2.id}", as: :json, headers: authenticated_header
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal [[20],[40]], resp.map {|d| d['cron_minute']}.sort {|d1,d2| d1[0] <=> d2[0]}
  end

  test "Dosage creation should build future notifications" do
    t = Time.now.strftime('%F %H:00 UTC').to_time
    post '/api/v1/dosages.json', params: {
      "dosage" => {
        "drug_id"=>@drug.id, "pacient_id"=>@pacient.id,
        "amount"=>2, "notification_type"=>0, "status"=>"in_treatment",
        "description"=>"pra ficar legal", "notes"=>"A bad pacient.",
        "start_at"=>(t - 2*60*60).to_s,
        "end_at"=>'',
        "cron_minute"=>"/10", "cron_hour"=>"", "cron_m_day"=>"", "cron_month"=>"", "cron_w_day"=>"",
        "total_dosages"=>5, "amount_unit"=>"drops"
      }
    }, headers: authenticated_header
    assert_response :success
    dosage = Dosage.last
    assert_equal [ t + 60*00, t + 60*10, t + 60*20, t + 60*30, t + 60*40 ],
                 dosage.notifications.map {|n| n.run_at.to_time.utc }.sort
  end

  test "Dosage update should rebuild future notifications" do
    # Set time points
    ch = Time.now.strftime('%F %H:00 UTC').to_time.utc # current hour
    m = 60
    h = 60*m
    two_hours_ago = ch - 2*h
    five_hours_future = ch + 5*h

    # Create dosage and its notifications
    dosage = create_dose [55], nil, nil, nil, nil, start_at: two_hours_ago.to_s, total_dosages: 5
    dosage.create_notifications two_hours_ago, five_hours_future

    # Confirm the initial notifications
    t = two_hours_ago
    assert_equal [ t+h*0+55*m, t+h*1+55*m, t+h*2+55*m, t+h*3+55*m, t+h*4+55*m ],
                 dosage.notifications.map {|n| n.run_at.to_time.utc}.sort

    # Update dosage and future notifications
    patch "/api/v1/dosages/#{dosage.id}.json",
          params: {"dosage" => {"status"=>"in_treatment", "cron_minute"=>"/10"}},
          headers: authenticated_header
    assert_response :success
    dosage.reload
    assert_equal [ t+h*0+55*m, t+h*1+55*m, ch+00*m, ch+10*m, ch+20*m ].join,
                 dosage.notifications.map {|n| n.run_at.to_time.utc}.sort.join
  end

end
