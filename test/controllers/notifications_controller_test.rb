require 'test_helper'

class NotificationsControllerTest < ActionDispatch::IntegrationTest
  min = 60
  half_hour = 30*min
  hour = 60*min

  setup do
    Minion.all.map &:destroy
    Notification.all.map &:destroy
    Minion.create key: 'MiNiOnKey'
    @pacient = Pacient.create nin: '123.456.789-00', name: 'Zé', phone: '12 3456 7890'
    @pacient.save!
    #@dose = Dosage.create cron_minute:[0], description: 'this is good.', pacient: @pacient
  end

  test "should block unknown minion" do
    Notification.create notifiable: @pacient, text: 'note 1', run_at: tzl_now
    get '/api/v1/message?key=unknownKey', as: :json
    assert_response :forbidden
  end

  test "should block when unset minion key" do
    Notification.create notifiable: @pacient, text: 'note 1', run_at: tzl_now
    get '/api/v1/message', as: :json
    assert_response :forbidden
  end

  test "admins should get all notifications" do
    Notification.create notifiable: @pacient, text: 'note 1', run_at: tzl_now, status: 'failed', minion_error: 'some error'
    Notification.create notifiable: @pacient, text: 'note 2', run_at: tzl_now
    get '/api/v1/notifications', as: :json, headers: authenticated_header
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal ['note 1', 'note 2'], resp.map {|m| m['text']}
    m1 = resp.find {|m| m['text'] == 'note 1' }
    assert_equal 'failed', m1['status']
    assert_equal 'some error', m1['minion_error']
  end

  test "should get messages" do
    # Time sets
    this_hour = tzl_now.strftime('%F %H:00 UTC').to_time
    oneandhalf = this_hour + 60*60*1.5
    # Generic notifications
    Notification.create notifiable: @pacient, text: 'note 1', run_at: tzl_now
    Notification.create notifiable: @pacient, text: 'note 2', run_at: tzl_now
    # Dosage notifications
    @drug = Drug.create! name: 'Remedy', concentration: '2mg', form: 'pill', description: "It's nice"
    dose = create_dose [30], [], [], [], [], start_at: this_hour
    dose.create_notifications this_hour, this_hour + 60*60
    # Appointment notifications
    doc = Collaborator.create! name: 'Doc', username: 'doc', password: 'test', nin: '000.000.000-99'
    appo = Appointment.create! pacient: @pacient, collaborator: doc, run_at: oneandhalf, description: 'Hello'
    appo.update_notification_pair

    # List messages for SMS
    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal [
      "#{this_hour.strftime('%H:30')} eh hora de 5 gotas de Remedy\nThis is a test.",
      "#{oneandhalf.strftime('%H:30')} eh hora da consulta.\nHello",
      'note 1', 'note 2'
    ], resp['messages'].map {|m| m['text']}.sort

    # List messages for App
    @pacient.app_key = 'someappkey'
    @pacient.notification_type = :app
    @pacient.save!
    get '/api/v1/message?pacientkey=someappkey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal [
      "#{this_hour.strftime('%H:30')} é hora de 5 gotas de Remedy\nThis is a test.",
      "#{oneandhalf.strftime('%H:30')} é hora da consulta.\nHello",
      'note 1', 'note 2'
    ], resp['messages'].map {|m| m['text']}.sort
  end

  test "should get SMS related data in message" do
    Notification.create notifiable: @pacient, text: 'some text', run_at: tzl_now
    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal 'some text', resp['messages'][0]['text']
    assert_equal '12 3456 7890', resp['messages'][0]['phone']
  end

  test "should work when no messages" do
    assert_equal 0, Notification.all.length
    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal 0, resp['messages'].length
  end

  test "should get messages in the default future limit" do
    Notification.create notifiable: @pacient, text: 'now', run_at: tzl_now
    Notification.create notifiable: @pacient, text: 'future limit', run_at: tzl_now + half_hour
    Notification.create notifiable: @pacient, text: 'future after limit', run_at: tzl_now + hour
    Notification.create notifiable: @pacient, text: 'past', run_at: tzl_now - half_hour
    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal ['past', 'now', 'future limit'], resp['messages'].map {|m| m['text']}
  end

  test "should get pending and failed messages" do
    Notification.create notifiable: @pacient, text: 'note 1', run_at: tzl_now, status: :pending
    Notification.create notifiable: @pacient, text: 'note 2', run_at: tzl_now, status: :failed, updated_at: Time.now.utc-6*min
    Notification.create notifiable: @pacient, text: 'note 3', run_at: tzl_now, status: :completed
    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal ['note 1', 'note 2'], resp['messages'].map {|m| m['text']}.sort
  end

  test "should get sending messages without update after 6 minutes" do
    Notification.create notifiable: @pacient, text: 'note 1', run_at: tzl_now, status: :sending, updated_at: Time.now.utc
    Notification.create notifiable: @pacient, text: 'note 2', run_at: tzl_now, status: :sending, updated_at: Time.now.utc-5*min
    Notification.create notifiable: @pacient, text: 'note 3', run_at: tzl_now, status: :sending, updated_at: Time.now.utc-6*min
    Notification.create notifiable: @pacient, text: 'note 4', run_at: tzl_now, status: :sending, updated_at: Time.now.utc-7*min
    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal ['note 3', 'note 4'], resp['messages'].map {|m| m['text']}.sort
  end

  test "should get failed messages to re-send after 6 minutes" do
    Notification.create notifiable: @pacient, text: 'note 1', run_at: tzl_now, status: :failed, updated_at: Time.now.utc
    Notification.create notifiable: @pacient, text: 'note 2', run_at: tzl_now, status: :failed, updated_at: Time.now.utc-5*min
    Notification.create notifiable: @pacient, text: 'note 3', run_at: tzl_now, status: :failed, updated_at: Time.now.utc-6*min
    Notification.create notifiable: @pacient, text: 'note 4', run_at: tzl_now, status: :failed, updated_at: Time.now.utc-7*min
    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal ['note 3', 'note 4'], resp['messages'].map {|m| m['text']}.sort
  end

  test "should not get too old notifications" do
    Notification.create notifiable: @pacient, text: 'note 1', run_at: tzl_now-110*min
    Notification.create notifiable: @pacient, text: 'note 2', run_at: tzl_now-119*min
    Notification.create notifiable: @pacient, text: 'note 3', run_at: tzl_now-120*min
    Notification.create notifiable: @pacient, text: 'note 4', run_at: tzl_now-130*min
    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal ['note 1', 'note 2'], resp['messages'].map {|m| m['text']}.sort
  end

  test "should count SMS fails" do
    n = Notification.create notifiable: @pacient, text: 'some text', run_at: tzl_now

    get "/api/v1/message/#{n.id}?key=MiNiOnKey&status=failed&error=bad%20thing", as: :json
    assert_equal 'failed', Notification.all.first.status
    assert_response :success
    assert_equal 1, Notification.all.first.fail_count
    n.updated_at = Time.now-6*min
    n.save!

    # Bad idea.
    #get '/api/v1/message?key=MiNiOnKey', as: :json
    #assert_response :success
    #assert_equal 'failed', Notification.all.first.status, 'must not change "failed" status to "sending"'

    get "/api/v1/message/#{n.id}?key=MiNiOnKey&status=failed&error=bad%20thing", as: :json
    assert_response :success
    assert_equal 2, Notification.all.first.fail_count

    get "/api/v1/message/#{n.id}?key=MiNiOnKey&status=failed&error=bad%20thing", as: :json
    assert_response :success
    assert_equal 3, Notification.all.first.fail_count
  end

  test "should not get messages that failed more than 5 times" do
    six_min_ago = Time.now.utc-6*min
    Notification.create notifiable: @pacient, text: 'note 1', run_at: tzl_now,
                        status: :failed, updated_at: six_min_ago
    Notification.create notifiable: @pacient, text: 'note 2', run_at: tzl_now,
                        status: :failed, updated_at: six_min_ago, fail_count: 4
    Notification.create notifiable: @pacient, text: 'note 3', run_at: tzl_now,
                        status: :failed, updated_at: six_min_ago, fail_count: 5
    Notification.create notifiable: @pacient, text: 'note 4', run_at: tzl_now,
                        status: :failed, updated_at: six_min_ago, fail_count: 6
    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal ['note 1', 'note 2', 'note 3'], resp['messages'].map {|m| m['text']}.sort
  end

  test "minions get only notification related to SMS enabled pacients" do
    Dosage.all.map &:destroy
    @drug = Drug.create! name: 'Remedy', concentration: '2mg', form: 'pill', description: "It's nice"
    nt_sms = Pacient.notification_types[:sms]
    nt_app = Pacient.notification_types[:app]
    p1 = Pacient.create nin: '111.111.111-11', name: 'Fulano', phone: '111', notification_type: nt_sms
    p1.save!
    p2 = Pacient.create nin: '222.222.222-22', name: 'Beltrano', phone: '222', notification_type: nt_app
    p2.save!
    p3 = Pacient.create nin: '333.333.333-33', name: 'Ciclano', phone: '333', notification_type: nt_sms
    p3.save!
    d1 = create_dose [10], nil, nil, nil, nil, amount: 1, pacient: p1
    d2 = create_dose [20], nil, nil, nil, nil, amount: 2, pacient: p2
    d3 = create_dose [30], nil, nil, nil, nil, amount: 3, pacient: p3

    Notification.create notifiable: p1, text: 'Note A', run_at: tzl_now
    Notification.create notifiable: d1, text: 'Note B', run_at: tzl_now
    Notification.create notifiable: p2, text: 'Note C', run_at: tzl_now
    Notification.create notifiable: d2, text: 'Note D', run_at: tzl_now
    Notification.create notifiable: p3, text: 'Note E', run_at: tzl_now
    Notification.create notifiable: d3, text: 'Note F', run_at: tzl_now

    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal ['Note A','Note B','Note E','Note F'], resp['messages'].map {|m| m['text']}.sort
  end

  test "should get messages in the default total limit" do
    15.times do |num|
      run_at = tzl_now + rand-0.5*20*min # Vary between -10 and +10 minutes
      Notification.create notifiable: @pacient, text: "nothing", run_at: run_at
    end
    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal 10, resp['messages'].length, 'the default total limit is 10'
  end

  test "ensure it pushes the older notice" do
    15.times do |num|
      Notification.create notifiable: @pacient, text: 'nothing', run_at: tzl_now
    end
    Notification.create notifiable: @pacient, text: 'older', run_at: tzl_now-min
    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal 10, resp['messages'].length
    assert_equal 'older', resp['messages'][0]['text'], 'the older must be the first'
  end

  test "should know the minion who get the notification" do
    Notification.create notifiable: @pacient, text: 'Some message', run_at: tzl_now
    assert_equal 1, Notification.all.length
    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    assert_equal 'MiNiOnKey', Notification.all.first.minion.key
  end

  test "should update notification status" do
    n = Notification.create notifiable: @pacient, text: 'Some message', run_at: tzl_now
    assert_equal 1, Notification.all.length
    assert_equal 'pending', Notification.all.first.status

    get '/api/v1/message?key=MiNiOnKey', as: :json
    assert_response :success
    assert_equal 'sending', Notification.all.first.status

    get "/api/v1/message/#{n.id}?key=MiNiOnKey&status=completed", as: :json
    assert_response :success
    assert_equal 'completed', Notification.all.first.status

    get "/api/v1/message/#{n.id}?key=MiNiOnKey&status=failed&error=bad%20thing", as: :json
    assert_response :success
    assert_equal 'failed', Notification.all.first.status
    assert_equal 'bad thing', Notification.all.first.minion_error
  end

  ### Paciente App #############################################################

  test 'Unset registerd pacient key get error ansswer' do
    get '/api/v1/message?pacientkey=unknownappkey', as: :json
    assert_response :forbidden
  end

  test 'Request for pacient notifications retrieve extra dosage data' do
    t = tzl_now.strftime('%F %H:00 UTC').to_time
    @drug = Drug.create name: 'Remedy', description: 'Nice remedy', concentration: '2mg', form: 'drops'
    dose = create_dose [5], nil, nil, nil, nil, start_at: t, description: 'to be good', pacient: @pacient
    @pacient.app_key = 'someappkey'
    @pacient.notification_type = :app
    @pacient.save!
    dose.create_notifications t, (t+5*60)
    get '/api/v1/message?pacientkey=someappkey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal 'Remedy', resp['messages'][0]['dosage']['drug_name']
    assert_equal 'to be good', resp['messages'][0]['dosage']['description']
  end

  test 'Request for pacient notifications retrieve only it self ones' do
    @drug = Drug.create name: 'remedy', description: 'Nice remedy', concentration: '2mg', form: 'drops'
    p2 = Pacient.create nin: '123.456.789-22', name: 'Fulano', phone: '11 1111 1111', app_key: 'otherappkey', notification_type: :app
    p2.save!
    dose = create_dose [20], nil, nil, nil, nil, amount: 2, pacient: @pacient
    @pacient.app_key = 'someappkey'
    @pacient.notification_type = :app
    @pacient.save!
    Notification.create notifiable: @pacient, text: 'Message A', run_at: tzl_now - 2*60*60
    Notification.create notifiable: dose,     text: 'Message B', run_at: tzl_now + 10
    Notification.create notifiable: p2,       text: 'Message C', run_at: tzl_now + 10
    Notification.create notifiable: @pacient, text: 'Message D', run_at: tzl_now + 2*60*60

    get '/api/v1/message?pacientkey=someappkey', as: :json
    assert_response :success
    resp = JSON.parse @response.body
    assert_equal ['Message B', 'Message D'], resp['messages'].map {|m| m['text']}.sort
  end

  test "should update notification status from pacient app" do
    n = Notification.create notifiable: @pacient, text: 'Some message', run_at: tzl_now
    assert_equal 1, Notification.all.length
    assert_equal 'pending', Notification.all.first.status
    @pacient.app_key = 'someappkey'
    @pacient.save!

    get "/api/v1/message/#{n.id}?pacientkey=someappkey&status=completed", as: :json
    assert_response :success
    assert_equal 'completed', Notification.all.first.status

    get "/api/v1/message/#{n.id}?pacientkey=someappkey&status=failed&error=bad%20thing", as: :json
    assert_response :success
    assert_equal 'failed', Notification.all.first.status
    assert_equal 'bad thing', Notification.all.first.minion_error
  end

  test "should update monitoring status from pacient app" do
    n = Notification.create notifiable: @pacient, text: 'Some message', run_at: tzl_now
    assert_equal 1, Notification.all.length
    assert_equal 'no_answer', Notification.all.first.monitoring
    @pacient.app_key = 'someappkey'
    @pacient.save!

    get "/api/v1/message/#{n.id}?pacientkey=someappkey&monitoring=done", as: :json
    assert_response :success
    assert_equal 'done', Notification.all.first.monitoring

    get "/api/v1/message/#{n.id}?pacientkey=someappkey&monitoring=not_done", as: :json
    assert_response :success
    assert_equal 'not_done', Notification.all.first.monitoring
  end

  test "stress" do
    if ENV['STRESS']
      test_hours = 20
      tBase = Time.new '2000-01-01'
      start = Time.now
      min = 60
      hour = min*60
      day = hour*24
      @sentSMS = {}
      def sendSMS(msg)
        id = msg['id']
        text = msg['text']
        puts "Send SMS: #{text.split("\n").join ' | '}"
        assert @sentSMS[text].nil?, "Notification #{id} was already sent."
        @sentSMS[text] = true
      end
      @appointmentCounter = 0
      def mkAppointment(run_at)
        @appointmentCounter += 1
        Appointment.create!(
          status: 'pending',
          run_at: run_at.strftime('%F %H:%M UTC').to_time,
          description: "Appointment #{@appointmentCounter}",
          collaborator: @doc, pacient: @pacient
        )
      end
      Minion.create key: 'OtherMiNiOnKey'
      @doc = Collaborator.create! name: 'Doc', username: 'doc', password: 'test', nin: '000.000.000-00'
      (test_hours*20).times do |h|
        a1 = mkAppointment Time.now + hour*h/20
        a2 = mkAppointment Time.now + day + hour*h/20
        a1.update_notification_pair
        a2.update_notification_pair
      end
      @drug = Drug.create! name: 'Remedy', concentration: '2mg', form: 'pill', description: "It's nice"
      start_at = (Time.now-hour).strftime '%F %H:00:00 UTC'
      15.times do |num|
        create_dose [-5], nil, nil, nil, nil, start_at: start_at, description: "Dosage #{num}", amount: num
      end
      tic = 0
      while Time.now < (start+hour*test_hours)
        puts "\nTic: #{tic} -- Now: #{Time.now.strftime '%H:%M:%S'} -- " +
             "Testing: #{(tBase+(Time.now-start)).strftime '%H:%M:%S'}"
        #puts 'Sent SMS:', @sentSMS.keys.map {|m| m.split("\n").join ' | '}.sort.join("\n")
        #puts '--------------------------------------------------------------------------------'
        if tic%10 == 0
          puts 'Update Notifications'
          Dosage.create_notifications tzl_now-hour*1, tzl_now+hour+2
        end
        minion = (rand<0.5) ? 'MiNiOnKey' : 'OtherMiNiOnKey'
        print "Minion #{minion} is requiring updates... "
        get '/api/v1/message?key='+minion, as: :json
        resp = JSON.parse @response.body
        puts resp['messages'].empty? ? 'no messages.' : 'got some!'
        resp['messages'].each do |msg|
          puts "Minion send #{msg['notifiable_type']} #{msg['id']} " +
               "run_at:#{msg['run_at'].to_time.strftime '%H:%M'}"
          if rand < 0.5
            sendSMS msg
            get "/api/v1/message/#{msg['id']}?key=MiNiOnKey&status=completed", as: :json
          elsif rand < 0.5
            puts 'Simulate fail.'
            get "/api/v1/message/#{msg['id']}?key=MiNiOnKey&status=failed&error=bad", as: :json
          else
            puts 'Simulate no response.'
          end
        end
        sleep 30 * rand
        tic += 1
      end
    else
      print '😴'
    end
  end

end
