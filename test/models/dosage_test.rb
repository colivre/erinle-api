require 'test_helper'

class DosageTest < ActiveSupport::TestCase

   def setup
     Dosage.all.map &:destroy
     Notification.all.map &:destroy
     @drug ||= Drug.create name: 'remedy', description: 'Nice remedy', concentration: '2mg', form: 'drops'
     @drug.save!
     @pacient ||= Pacient.create name: 'Zé', nin: '111.222.333-44', phone: '12-3456-7890'
     @pacient.save!
   end

   def assert_should_notify_for_each_5min(dose, tstart, tend, list_ok)
     assert_should_notify_for_each 60*5, dose, tstart, tend, list_ok
   end

   def assert_should_notify_for_each_hour(dose, tstart, tend, list_ok)
     assert_should_notify_for_each 60*60, dose, tstart, tend, list_ok
   end

   def assert_should_notify_for_each_day(dose, tstart, tend, list_ok)
     assert_should_notify_for_each 60*60*24, dose, tstart, tend, list_ok
   end

   def assert_should_notify_for_each(inc, dose, tstart, tend, list_ok)
     tstart += ' 00:00' unless tstart =~ /[0-9]{2}:[0-9]{2}/
     tend += ' 00:00' unless tend =~ /[0-9]{2}:[0-9]{2}/
     t = (tstart+' UTC').to_time
     tend = (tend+' UTC').to_time
     list_ok = list_ok.map do |d|
       d += ' 00:00' unless d =~ /[0-9]{2}:[0-9]{2}/
       (d+' UTC').to_time
     end
     while t <= tend
       if list_ok.include? t
         assert dose.should_notify_at?(t), "It should notify at #{t}."
       else
         refute dose.should_notify_at?(t), "It should NOT notify at #{t}."
       end
       t += inc
     end
   end

   test "cron_minute must be divisibe by 5" do
     assert create_dose [0], [12], nil, nil, nil, amount: 1
     assert create_dose [5], [12], nil, nil, nil, amount: 2
     assert create_dose [5,10], [12], nil, nil, nil, amount: 3
     assert_raise(StandardError) { create_dose [3], [12], nil, nil, nil }
     assert_raise(StandardError) { create_dose [5,8], [12], nil, nil, nil }
   end

   test "cron_w_day must not be negative (cron interval)" do
     assert create_dose [0], nil, nil, [0], nil, amount: 1
     assert create_dose [0], nil, nil, [1,3], nil, amount: 2
     assert_raise(StandardError) { create_dose [5,8], nil, nil, [-2], nil }
     assert_raise(StandardError) { create_dose [5,8], nil, nil, [1,-2], nil }
   end

   test "Match date on simple minute list" do
     dose = create_dose [0, 10, 20], nil, nil, nil, nil
     assert_should_notify_for_each_5min dose, '2020/01/01 12:00', '2020/01/01 13:00', [
       '2020/01/01 12:00', '2020/01/01 12:10', '2020/01/01 12:20', '2020/01/01 13:00'
     ]
   end

   test "Match date on simple minute list and respect the start_at attr" do
     dose = create_dose [0, 10, 20], nil, nil, nil, nil, start_at: '2020/01/01 12:00'
     assert_should_notify_for_each_5min dose, '2020/01/01 10:00', '2020/01/01 13:00', [
       '2020/01/01 12:00', '2020/01/01 12:10', '2020/01/01 12:20', '2020/01/01 13:00'
     ]
   end

   test "Match date on minute interval" do
     dose = create_dose [-50], nil, nil, nil, nil, start_at: '2020/01/01 12:00'
     assert_should_notify_for_each_5min dose, '2020/01/01 10:00', '2020/01/01 15:00', [
       '2020/01/01 12:00', '2020/01/01 12:50', '2020/01/01 13:40', '2020/01/01 14:30'
     ]
   end

   test "Match date on minute interval with hour list" do
     dose = create_dose [-50], [12,13,14], nil, nil, nil, start_at: '2020/01/01 12:00'
     assert_should_notify_for_each_5min dose, '2020/01/01 00:00', '2020/01/02 00:00', [
       '2020/01/01 12:00', '2020/01/01 12:50', '2020/01/01 13:40', '2020/01/01 14:30'
     ]
   end

   test "Match date on simple hour list" do
     dose = create_dose [0], [10,20], nil, nil, nil
     assert_should_notify_for_each_5min dose, '2020/01/01 00:00', '2020/01/03 00:00', [
       '2020/01/01 10:00', '2020/01/01 20:00', '2020/01/02 10:00', '2020/01/02 20:00'
     ]
   end

   test "Match date on hour interval, starting in 0" do
     dose = create_dose [0], [-10], nil, nil, nil, start_at: '2020-01-01 00:00'
     assert_should_notify_for_each_5min dose, '2020/01/01 01:00', '2020/01/03 00:00', [
       '2020/01/01 10:00', '2020/01/01 20:00', '2020/01/02 06:00', '2020/01/02 16:00'
     ]
   end

   test "Match date on hour interval, starting out 0" do
     dose = create_dose [0], [-10], nil, nil, nil, start_at: '2020-01-01 03:00'
     assert_should_notify_for_each_5min dose, '2020/01/01 04:00', '2020/01/03 00:00', [
       '2020/01/01 13:00', '2020/01/01 23:00', '2020/01/02 09:00', '2020/01/02 19:00'
     ]
   end

   test "Match date on simple month day list" do
     dose = create_dose [0], [0], [10, 20], nil, nil
     assert_should_notify_for_each_hour dose, '2020/01/01 00:00', '2020/03/01 00:00', [
       '2020-01-10', '2020-01-20', '2020-02-10', '2020-02-20'
     ]
   end

   test "Match date on month day interval" do
     dose = create_dose [0], [0], [-14], nil, nil, start_at: '2020-01-01' # Each two weeks
     assert_should_notify_for_each_hour dose, '2020/01/02 00:00', '2020/03/01 00:00', [
       '2020-01-15', '2020-01-29', '2020-02-12', '2020-02-26'
     ]
   end

   test "Match date on month day interval list" do
     dose = create_dose [0], [0], [-14, -29], nil, nil, start_at: '2020-01-01'
     assert_should_notify_for_each_hour dose, '2020/01/02 00:00', '2020/03/01 00:00', [
       '2020-01-15', '2020-01-29', '2020-01-30', '2020-02-12', '2020-02-26', '2020-02-28'
     ]
   end

   test "Match date on first month day and month day interval" do
     dose = create_dose [0], [0], [1, -14], nil, nil, start_at: '2020-01-01'
     assert_should_notify_for_each_hour dose, '2020/01/01 00:00', '2020/02/29 00:00', [
       '2020-01-01', '2020-01-15', '2020-01-29', '2020-02-01', '2020-02-12', '2020-02-26'
     ]
   end

   test "Match date on simple week day list" do
     dose = create_dose [0], [0], nil, [1,3,5], nil # Monday, Wednesday and Friday.
     assert_should_notify_for_each_hour dose, '2020/01/05 00:00', '2020/01/11 00:00', [
       '2020-01-06', '2020-01-08', '2020-01-10'
     ]
   end

   test "Match date on simple month list" do
     dose = create_dose [0], [0], [10], nil, [2,4,6]
     assert_should_notify_for_each_day dose, '2020/01/01 00:00', '2021/01/01 00:00', [
       '2020-02-10', '2020-04-10', '2020-06-10'
     ]
   end

   test "Match date on simple month and day list" do
     dose = create_dose [0], [0], [10,20], nil, [2,4]
     assert_should_notify_for_each_day dose, '2020/01/01 00:00', '2021/01/01 00:00', [
       '2020-02-10', '2020-02-20', '2020-04-10', '2020-04-20'
     ]
   end

   test 'create notification at some valid date' do
     dose = create_dose [0], [12], nil, nil, nil, {
       start_at: '2020/01/01 00:00', amount: 2, description: 'this is good!'
     }
     n = dose.create_notification_at '2020/01/01 12:00 UTC'.to_time
     assert_equal "12:00 é hora de 2 gotas de remedy\nthis is good!", n.text
   end

   test 'create notifications for all doses in a time interval' do
     Notification.all.map &:destroy
     create_dose [0], [-2], nil, nil, nil, description: 'dose 1', amount: 1
     create_dose [-30], [3], nil, nil, nil, description: 'dose 2', amount: 2
     Dosage.create_notifications '2020/01/01 00:00 UTC'.to_time, '2020/01/01 04:00 UTC'.to_time
     assert_equal [
       "2020-01-01 00:00 | 00:00 é hora de 1 gotas de remedy;dose 1",
       "2020-01-01 02:00 | 02:00 é hora de 1 gotas de remedy;dose 1",
       "2020-01-01 03:00 | 03:00 é hora de 2 gotas de remedy;dose 2",
       "2020-01-01 03:30 | 03:30 é hora de 2 gotas de remedy;dose 2",
       "2020-01-01 04:00 | 04:00 é hora de 1 gotas de remedy;dose 1"
     ], Notification.all.map {|n| "#{n.run_at.strftime '%F %H:%M'} | #{n.text.tr("\n",";")}" }
   end

   test 'create notifications for all doses next # hours interval' do
     Notification.all.map &:destroy
     now = Time.now.strftime('%F %H:%M UTC').to_time
     t = now.strftime( '%%F %%H:%02d UTC' % ( now.min - now.min%5 ) ).to_time
     assert t <= now, 'The time adjust must be equal or before now'
     assert t >= (now - 5*60), 'The time adjust must not be bigger than 5 minutes'
     create_dose [-45], nil, nil, nil, nil, description: 'dose 1', start_at: t.strftime('%F %H:%M'), amount: 1
     create_dose [-50], nil, nil, nil, nil, description: 'dose 2', start_at: t.strftime('%F %H:%M'), amount: 2
     Dosage.create_notifications_to_next_hours 2.5
     time_format = '%F %H:%M | %H:%M'
     assert_equal [
       "#{(t+ 00*60).strftime time_format} é hora de 1 gotas de remedy;dose 1",
       "#{(t+ 00*60).strftime time_format} é hora de 2 gotas de remedy;dose 2",
       "#{(t+ 45*60).strftime time_format} é hora de 1 gotas de remedy;dose 1",
       "#{(t+ 50*60).strftime time_format} é hora de 2 gotas de remedy;dose 2",
       "#{(t+ 90*60).strftime time_format} é hora de 1 gotas de remedy;dose 1",
       "#{(t+100*60).strftime time_format} é hora de 2 gotas de remedy;dose 2",
       "#{(t+135*60).strftime time_format} é hora de 1 gotas de remedy;dose 1",
       "#{(t+150*60).strftime time_format} é hora de 2 gotas de remedy;dose 2"
     ], Notification.all.map {|n| "#{n.run_at.strftime '%F %H:%M'} | #{n.text.tr("\n",";")}" }.sort
   end

   test 'respect notification count limit' do
     Notification.all.map &:destroy
     create_dose [-20], nil, nil, nil, nil, total_dosages: 5
     Dosage.create_notifications '2020/01/01 00:00 UTC'.to_time, '2020/01/01 03:00 UTC'.to_time
     assert_equal [
       "2020-01-01 00:00",
       "2020-01-01 00:20",
       "2020-01-01 00:40",
       "2020-01-01 01:00",
       "2020-01-01 01:20"
     ], Notification.all.map {|n| n.run_at.strftime '%F %H:%M' }.sort
   end

   test 'respect notification date limit' do
     Notification.all.map &:destroy
     create_dose [-20], nil, nil, nil, nil, end_at: '2020/01/01 01:20 UTC'
     Dosage.create_notifications '2020/01/01 00:00 UTC'.to_time, '2020/01/01 03:00 UTC'.to_time
     assert_equal [
       "2020-01-01 00:00",
       "2020-01-01 00:20",
       "2020-01-01 00:40",
       "2020-01-01 01:00",
       "2020-01-01 01:20"
     ], Notification.all.map {|n| n.run_at.strftime '%F %H:%M' }.sort
   end

   test 'must not repeat dosages with same drug, and dosage details to the same pacient' do
     pacient2 ||= Pacient.create name: 'Fulano', nin: '999.999.999-99', phone: '99-9999-9999'
     pacient2.save!
     create_dose [0], nil, nil, nil, nil,
                 pacient:@pacient, drug:@drug, amount: 1, amount_unit:'drops'
     create_dose [0], nil, nil, nil, nil,
                 pacient:@pacient, drug:@drug, amount: 2, amount_unit:'drops'
     assert_raises(Exception) { create_dose [0], nil, nil, nil, nil,
                 pacient:@pacient, drug:@drug, amount: 1, amount_unit:'drops' }
     create_dose [0], nil, nil, nil, nil,
                 pacient:pacient2, drug:@drug, amount: 1, amount_unit:'drops'
     assert_equal 3, Dosage.all.length
   end

   test 'finish notifications after end_at' do
     d1 = create_dose [0], nil, nil, nil, nil,
          pacient:@pacient, drug:@drug, amount: 1, amount_unit:'drops', end_at: (Time.now-90).strftime('%F %H:%M UTC')
     d2 = create_dose [0], nil, nil, nil, nil,
          pacient:@pacient, drug:@drug, amount: 2, amount_unit:'drops', end_at: (Time.now+90).strftime('%F %H:%M UTC')
     d1.reload
     d2.reload
     assert_equal 'in_treatment', d1.status.to_s
     assert_equal 'in_treatment', d2.status.to_s
     Dosage.finish_notifications_after_end_at
     d1.reload
     d2.reload
     assert_equal 'finished', d1.status.to_s
     assert_equal 'in_treatment', d2.status.to_s
   end

end
