require 'test_helper'

class DrugTest < ActiveSupport::TestCase

  def setup
    Drug.all.map &:destroy
  end

  test 'must not repeat the same drug composition' do
    Drug.create! name: 'RemedyA', concentration: '2mg', form: 'pill', description: "It's nice"
    Drug.create! name: 'RemedyA', concentration: '3mg', form: 'pill', description: "It's strong"
    Drug.create! name: 'RemedyB', concentration: '2mg', form: 'pill', description: "It's ok"
    assert_equal 3, Drug.all.length
    assert_raise (ActiveRecord::RecordNotUnique) {
      Drug.create! name: 'RemedyA', concentration: '2mg', form: 'pill', description: "It's a repetition"
    }
  end

end
