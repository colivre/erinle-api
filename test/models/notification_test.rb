require 'test_helper'

class NotificationTest < ActiveSupport::TestCase

  def setup
    Notification.all.map &:destroy
    @pacient = Pacient.create name: 'Zé', phone: '12 3456 7890'
    @dose = Dosage.create cron_minute:[0],
      start_at: '2020-01-01 00:00 UTC'.to_time, description: 'this is good.', pacient: @pacient
  end

  test "Create with automatic pending status" do
    n = Notification.create notifiable: @pacient, run_at: Time.now.utc
    assert_equal 'pending', n.status
  end

  test "Create with a text" do
    n = Notification.create notifiable: @pacient, text: 'This is a test.', run_at: Time.now.utc
    assert_equal 'This is a test.', n.text
  end

  test "notification can have no minion relation" do
    n = Notification.create notifiable: @pacient, run_at: Time.now.utc
    n.save!
    assert_nil n.minion
  end

  test "notification can show its pacient with direct reference" do
    n = Notification.create notifiable: @pacient, run_at: Time.now.utc
    n.save!
    assert 'Zé', n.pacient.name
  end

  test "notification can find its pacient with indirect reference" do
    n = Notification.create notifiable: @dose, run_at: Time.now.utc
    n.save!
    assert 'Zé', n.pacient.name
  end

  test 'scope notification by status' do
    num = 0
    (%w{pending sending completed failed}*2).each do |status|
      num += 1
      Notification.create notifiable: @dose, status: status, run_at: Time.now.utc, text: status+num.to_s
    end
    assert_equal %w{pending1 pending5}, Notification.pending.map(&:text)
    assert_equal %w{sending2 sending6}, Notification.sending.map(&:text)
    assert_equal %w{completed3 completed7}, Notification.completed.map(&:text)
    assert_equal %w{failed4 failed8}, Notification.failed.map(&:text)
  end

end
