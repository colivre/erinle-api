require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # TimeZone Less current time
  def tzl_now(inc=0)
    (Time.now+inc).strftime('%F %H:%M UTC').to_time
  end

  def create_dose(min, hour, m_day, w_day, month, opts={})
    opts[:drug] ||= @drug
    opts[:pacient] ||= @pacient
    opts[:amount] ||= 5
    opts[:amount_unit] ||= 'gotas'
    opts[:total_dosages] ||= 0 # no limit
    opts[:description] ||= 'This is a test.'
    if opts[:start_at].blank?
      opts[:start_at] = '2020/01/01 00:00'
      opts[:start_at] += ' 00:00' unless opts[:start_at] =~ /[0-9]{2}:[0-9]{2}/
      opts[:start_at] = (opts[:start_at]+' UTC').to_time
    end
    opts[:end_at] ||= '2030/01/01 00:00'
    opts[:end_at] += ' 00:00' unless opts[:end_at] =~ /[0-9]{2}:[0-9]{2}/
    opts[:end_at] = (opts[:end_at]+' UTC').to_time
    dose = Dosage.create(opts.merge(
      cron_minute: min   || [],
      cron_hour:   hour  || [],
      cron_m_day:  m_day || [],
      cron_w_day:  w_day || [],
      cron_month:  month || []
    ))
    dose.save!
    dose
  end

  def authenticated_header user=nil
    if user == nil
      unless @auth_user
        @auth_user = Collaborator.create name: 'Zé', username: 'ze', password: 'test', nin: '000.000.000-00', administrator: true
        @auth_user.save!
      end
      user = @auth_user
    end
    token = Knock::AuthToken.new(payload: { sub: user.id }).token
    { 'Authorization': "Bearer #{token}" }
  end

end
